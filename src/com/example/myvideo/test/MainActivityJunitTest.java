package com.example.myvideo.test;

import static org.junit.Assert.*;
import com.example.myvideo.MainActivity;
import com.example.myvideo.R.id;
import android.support.test.espresso.*;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

public class MainActivityJunitTest extends ActivityInstrumentationTestCase2<MainActivity> {
	private MainActivity mTstMainActivity;
	private EditText txtVideoFilePath;
	
	public MainActivityJunitTest() {
		super(MainActivity.class);		
		// TODO Auto-generated constructor stub
	}
	
	public MainActivityJunitTest(String pkg, Class<MainActivity> activityClass) {
		super(activityClass);		
		// TODO Auto-generated constructor stub
	}

	@Before
	protected void setUp() throws Exception {
		super.setUp();
	       // injectInstrumentation(InstrumentationRegistry.getInstrumentation());
	        mTstMainActivity = getActivity();
	        Espresso.onView(ViewMatchers.withId(id.editText1)).check(ViewAssertions.matches(ViewMatchers.withText("/samples/")));
	        Espresso.onView(ViewMatchers.withId(id.button1)).perform(ViewActions.click()) ;
	        Espresso.onView(ViewMatchers.withId(id.button2)).perform(ViewActions.click()) ;
	        //(TextView) mFirstTestActivity.findViewById(R.id.my_first_test_text_view);
	}

	@After
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testDisplayVideoActivity(){
		 
		 // register next activity that need to be monitored.R.
		  /*ActivityMonitor activityMonitor = getInstrumentation().addMonitor(NextActivity.class.getName(), null, false);
		  NextActivity nextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
		  // next activity is opened and captured.
		  assertNotNull(nextActivity);
		  nextActivity .finish();
		  */
		 Espresso.onView(ViewMatchers.withId(id.play_button)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
		 //fail("Not yet implemented");
	 }

}
